drop table if exists household;
drop table if exists booking;

create table household
(
    ID       bigint      not null AUTO_INCREMENT,
    username varchar(20) not null,
    password varchar(64) not null,
    PRIMARY KEY (ID)
);

create table booking
(
    ID           bigint     not null AUTO_INCREMENT,
    household_id bigint     not null,
    laundry_room varchar(2) not null,
    booking_date date       not null,
    booking_hour int        not null,
    PRIMARY KEY (ID),
    FOREIGN KEY (household_id) REFERENCES household (id),
    CONSTRAINT chk_room CHECK (laundry_room IN ('R1', 'R2')),
    CONSTRAINT chk_hour CHECK (booking_hour >= 7 AND booking_hour <= 22)
);

CREATE INDEX index1 ON booking (booking_date);
CREATE UNIQUE INDEX index2 ON booking (booking_date, booking_hour, laundry_room);
