package com.volvo.booker;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@ToString(exclude = {"password", "token"})
@EqualsAndHashCode(exclude = {"password", "token"})
public class Household {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String username;

    private String password;

    @Transient
    private String token;

    protected Household() {
    }

    public Household(String username) {
        this.username = username;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String getUsername() {
        return username;
    }

}
