package com.volvo.booker;

import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface BookingRepository extends CrudRepository<Booking, Long> {
    List<Booking> findByHousehold(Household household);
    List<Booking> findByDateAndHourAndLaundryRoom(LocalDate date, int hour, LaundryRoom room);
    List<Booking> findByDateBetween(LocalDate from, LocalDate to);
}
