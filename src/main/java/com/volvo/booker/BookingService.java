package com.volvo.booker;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.volvo.booker.Booking.HOUR_RANGE;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;

@Service
@SpringBootApplication
public class BookingService {
    private final BookingRepository bookingRepo;
    private final HouseholdRepository householdRepo;

    // TODO make clock injectable so it can be mocker for testing
    private final Clock clock = Clock.systemDefaultZone();

    public BookingService(BookingRepository bookingRepo, HouseholdRepository householdRepo) {
        this.bookingRepo = bookingRepo;
        this.householdRepo = householdRepo;
    }

    public Booking book(Booking booking) {
        SecurityService.validateToken(booking);
        List<Booking> bookings = bookingRepo.findByDateAndHourAndLaundryRoom(
                booking.getDate(), booking.getHour(), booking.getLaundryRoom());
        if (!bookings.isEmpty()) {
            throw new RuntimeException("Time slot is not available");
        }
        try {
            return bookingRepo.save(booking);
        } catch (DataIntegrityViolationException e) {
            // TODO fix: this could be other DB constraint
            throw new RuntimeException("Time slot is not available");
        }
    }

    public List<Booking> listBookings(Household houseHold) {
        SecurityService.validateToken(houseHold);
        return bookingRepo.findByHousehold(houseHold);
    }

    public void cancelBooking(Booking booking) {
        SecurityService.validateToken(booking);
        bookingRepo.delete(booking);
    }

    public Household login(String username, String password) {
        Household household = householdRepo.findByUsernameAndPassword(username, password);
        if (household == null) {
            throw new RuntimeException("Invalid username or password");
        }
        household.setToken(SecurityService.createToken(username));
        return household;
    }

    /**
     * shows the availability for one week.
     */
    public void printWeekAvailability(int weekDelta) {
        LocalDate today = LocalDate.now(clock).plusWeeks(weekDelta);
        LocalDate monday = today.with(previousOrSame(DayOfWeek.MONDAY));
        LocalDate sunday = today.with(nextOrSame(DayOfWeek.SUNDAY));
        List<Booking> weekBookings = bookingRepo.findByDateBetween(monday, sunday);
        System.out.print("   ");
        for (var day = monday; day.compareTo(sunday) <= 0; day = day.plusDays(1)) {
            System.out.printf("%s", day.format(DateTimeFormatter.ofPattern(" MM/dd ")));
        }
        System.out.printf("%n");
        for (var hour = HOUR_RANGE.getMinimum(); hour <= HOUR_RANGE.getMaximum(); hour++) {
            System.out.printf("%02d ", hour);
            for (var day = monday; day.compareTo(sunday) <= 0; day = day.plusDays(1)) {
                Integer finalHour = hour;
                LocalDate finalDay = day;
                boolean available1 = weekBookings.stream().noneMatch(b -> match(b, LaundryRoom.R1, finalHour, finalDay));
                boolean available2 = weekBookings.stream().noneMatch(b -> match(b, LaundryRoom.R2, finalHour, finalDay));
                System.out.printf(available1 ? "   " : "  1");
                System.out.printf(available2 ? "    " : " 2  ");
            }
            System.out.printf("%n");
        }
    }

    private boolean match(Booking b, LaundryRoom room, Integer hour, LocalDate date) {
        return b.getLaundryRoom().equals(room) && b.getHour().equals(hour) && b.getDate().equals(date);
    }

}
