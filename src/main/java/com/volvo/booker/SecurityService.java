package com.volvo.booker;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.codec.digest.DigestUtils;

public class SecurityService {

    public static final String SECRET = "secret";
    private static final Algorithm algorithm = Algorithm.HMAC256(SECRET);
    public static final String ISSUER = "volvo";
    private static final JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(ISSUER)
            .build();

    public static String createToken(String username) {
        try {
            return JWT.create()
                    .withClaim("username", username)
                    .withIssuer(ISSUER)
                    .sign(algorithm);
        } catch (JWTCreationException e) {
            throw new RuntimeException(e);
        }
    }

    public static void validateToken(Household household) {
        validateToken(household.getToken(), household.getUsername());
    }

    public static void validateToken(Booking booking) {
        Household household = booking.getHousehold();
        validateToken(household.getToken(), household.getUsername());
    }

    public static void validateToken(String token, String username) {
        try {
            DecodedJWT jwt = verifier.verify(token);
            if (!jwt.getClaim("username").asString().equals(username)) {
                throw new RuntimeException("Not authorized");
            }
        } catch (JWTVerificationException e) {
            throw new RuntimeException(e);
        }
    }

    public static String hash(String password) {
        return new DigestUtils("SHA3-256").digestAsHex(password);
    }
}
