package com.volvo.booker;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.Range;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@ToString
@EqualsAndHashCode
public class Booking {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Household household;

    @Enumerated(EnumType.STRING)
    private LaundryRoom laundryRoom;
    @Column(name = "booking_date")
    private LocalDate date;
    @Column(name = "booking_hour")
    private Integer hour;

    public static final Range<Integer> HOUR_RANGE = Range.between(7, 22);

    protected Booking() {
    }

    public Booking(Household household, LaundryRoom laundryRoom, LocalDate date, Integer hour) {
        this.household = household;
        this.laundryRoom = laundryRoom;
        this.date = date;
        this.hour = hour;
        if (!HOUR_RANGE.contains(hour)) {
            throw new RuntimeException(String.format("Invalid hour: %d. Must be in range %s", hour, HOUR_RANGE));
        }
    }

    public Household getHousehold() {
        return household;
    }

    public LaundryRoom getLaundryRoom() {
        return laundryRoom;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getHour() {
        return hour;
    }
}
