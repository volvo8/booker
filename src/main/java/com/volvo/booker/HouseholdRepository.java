package com.volvo.booker;

import org.springframework.data.repository.CrudRepository;

public interface HouseholdRepository extends CrudRepository<Household, Long> {
    Household findByUsernameAndPassword(String username, String password);
}
