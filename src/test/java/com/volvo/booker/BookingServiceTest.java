package com.volvo.booker;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BookingService.class)
@AutoConfigureTestDatabase
class BookingServiceTest {

    @Autowired
    private BookingService bs;

    /**
     * This unit tests is more of a basic demo of the system.
     * It should be split in many smaller unit tests for each case.
     * It should inject a fixed clock to BookingService to be consistent.
     */
    @Test
    void book() {
        System.out.println("initial state -> no bookings, full availability");
        bs.printWeekAvailability(0);
        var house1 = bs.login("house1", SecurityService.hash("password1"));
        assertEquals(0, bs.listBookings(house1).size());

        System.out.println("house1 books room 1 today at 7");
        var b1 = bs.book(new Booking(house1, LaundryRoom.R1, LocalDate.now(), 7));
        assertEquals(List.of(b1), bs.listBookings(house1));
        bs.printWeekAvailability(0);

        System.out.println("house1 tries to book room 1 today at 6");
        RuntimeException e = assertThrows(
                RuntimeException.class,
                () -> bs.book(new Booking(house1, LaundryRoom.R1, LocalDate.now(), 6)));
        assertEquals("Invalid hour: 6. Must be in range [7..22]", e.getMessage());

        System.out.println("house1 tries to book room 1 today at 7 again");
        e = assertThrows(
                RuntimeException.class,
                () -> bs.book(new Booking(house1, LaundryRoom.R1, LocalDate.now(), 7)));
        assertEquals("Time slot is not available", e.getMessage());

        System.out.println("house1 books room 2 today at 7");
        var b2 = bs.book(new Booking(house1, LaundryRoom.R2, LocalDate.now(), 7));
        assertEquals(List.of(b1, b2), bs.listBookings(house1));
        bs.printWeekAvailability(0);

        System.out.println("house1 cancels booking room 2 today at 7");
        bs.cancelBooking(b2);
        assertEquals(List.of(b1), bs.listBookings(house1));
        bs.printWeekAvailability(0);
    }
}
